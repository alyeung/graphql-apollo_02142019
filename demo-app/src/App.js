import React from 'react';

import { CarToolQuery } from './queries/cars/CarToolQuery';
import { WidgetTool } from './components/widgets/WidgetTool';

export const App = () => {
  return <>
    <WidgetTool />
    <CarToolQuery />
  </>;
};
