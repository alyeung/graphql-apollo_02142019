import React from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';

import { CarForm } from '../../components/cars/CarForm';

const carsQuery = gql`
  query Cars {
    cars {
      id
      make
      model
      year
      color
      price
    }
  }
`;

const appendCarMutation = gql`
  mutation AppendCar($car: AppendCar!) {
    appendCar(car: $car) {
      id
      make
      model
      year
      color
      price
    }
  }
`;

export const CarFormMutation = () => {

  return <Mutation mutation={appendCarMutation}>
    {mutate => {

      const appendCar = car => {

        mutate({
          mutation: appendCarMutation,
          variables: { car },
          optimisticResponse: {
            appendCar: {
              id: -1,
              ...car,
              __typename: 'Car'
            },
          },
          update: (store, { data: { appendCar: car }}) => {
            let data = store.readQuery({ query: carsQuery });
            data.cars.push(car);
            store.writeQuery({ query: carsQuery, data });
          },
        });

      };

      return <CarForm buttonText="Add Car" onSubmitCar={appendCar} />;

    }}
  </Mutation>;

};