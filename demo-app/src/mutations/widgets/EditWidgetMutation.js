import React from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';

import { DeleteWidgetMutation } from './DeleteWidgetMutation';

const editWidgetMutation = gql`
  mutation EditWidget($widgetId: Int!) {
    setEditWidgetId(widgetId: $widgetId) @client
  }
`;

export const EditWidgetMutation = (props) => {

  return <Mutation mutation={editWidgetMutation}>
    {mutate => {

      const editWidget = widgetId => {

        console.log('called edit widget', widgetId);

        mutate({
          variables: { widgetId },
        }).then(results => {
          console.log('success: ', results);
        }).catch(results => {
          console.log('error',results);
        });

      };

      return <DeleteWidgetMutation onEditWidget={editWidget} {...props} />;

    }}
  </Mutation>;

};