import React from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

import { CarTool } from '../../components/cars/CarTool';

const carsQuery = gql`
  query Cars {
    cars {
      id
      make
      model
      year
      color
      price
    }
  }
`;

export const CarToolQuery = () => {

  return <Query query={carsQuery}>
    {( { loading, error, data } ) => {

      if (loading) return 'Loading';
      if (error) return 'Error';

      console.log(data.cars);

      return <CarTool cars={data.cars} />;

    }}
  </Query>;


};