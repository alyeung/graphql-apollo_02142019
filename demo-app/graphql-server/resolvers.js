import fetch from 'node-fetch';

export const resolvers = {
  Query: {
    widgets: async (_1, _2, context) => {
      const res = await fetch(`${context.restURL}/widgets`);
      return await res.json();
    },
    widget: async (_, { widgetId }, { restURL }) => {
      const escapedWidgetId = encodeURIComponent(widgetId);
      const res = await fetch(`${restURL}/widgets/${escapedWidgetId}`);
      return await res.json();
    },
    cars: async (_1, _2, context) => {
      const res = await fetch(`${context.restURL}/cars`);
      return await res.json();
    },
    car: async (_, { carId }, { restURL }) => {
      const escapedCarId = encodeURIComponent(carId);
      const res = await fetch(`${restURL}/cars/${escapedCarId}`);
      return await res.json();
    },
    authors: async (_1, _2, context) => {
      const res = await fetch(`${context.restURL}/authors`);
      return await res.json();
    },
    books: async (_1, _2, context) => {
      const res = await fetch(`${context.restURL}/books`);
      return await res.json();
    },
    contacts: () => {

      return [
        { id: 1, firstName: 'Bob', lastName: 'Smith', ssn: '111-111-1111' },
        { id: 1, firstName: 'Lisa', lastName: 'Timmons', companyName: 'Acme, Inc.', ein: '12-1212122' },
      ];

    },
    people: () => {

      return [
        { id: 1, firstName: 'Bob', lastName: 'Smith', ssn: '111-111-1111' },
        { id: 1, firstName: 'Lisa', lastName: 'Timmons', companyName: 'Acme, Inc.', ein: '12-1212122' },
      ];

    },
  },
  Contact: {
    __resolveType: (obj) => {
      if (obj.ssn) {
        return 'Employee';
      }
      if (obj.ein) {
        return 'Vendor';
      }
      return null;
    }
  },
  People: {
    __resolveType: (obj) => {
      if (obj.ssn) {
        return 'Employee';
      }
      if (obj.ein) {
        return 'Vendor';
      }
      return null;
    }
  },
  Book: {
    author: async ({ authorId }, _, { restURL }) => {
      const res = await fetch(`${restURL}/authors/${encodeURIComponent(authorId)}`);
      return await res.json();
    },
  },
  Author: {
    // default resolver
    firstName: (author) => author.firstName,
    fullName: (author) => {
      return author.firstName + ' ' + author.lastName;
    },
    bookCount: async (author, _, { restURL }) => {
      const res = await fetch(`${restURL}/books?authorId=${encodeURIComponent(author.id)}`);
      const books = await res.json();
      return books.length;
    },
    books: async (author, _, { restURL }) => {
      const res = await fetch(`${restURL}/books?authorId=${encodeURIComponent(author.id)}`);
      return await res.json();
    },
  },
  Mutation: {
    appendWidget: async (_, { widget }, { restURL }) => {
      const res = await fetch(`${restURL}/widgets`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(widget),
      });
      return await res.json();
    },
    appendCar: async (_, { car }, { restURL }) => {
      const res = await fetch(`${restURL}/cars`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(car),
      });
      return await res.json();
    },
    deleteWidget: async (_, { widgetId }, { restURL }) => {

      const escapedWidgetId = encodeURIComponent(widgetId);
      const res = await fetch(`${restURL}/widgets/${escapedWidgetId}`);
      const oldWidget = await res.json();

      await fetch(`${restURL}/widgets/${escapedWidgetId}`, {
        method: 'DELETE',
      });

      return oldWidget;
    },
    deleteCar: async (_, { carId }, { restURL }) => {

      const escapedCarId = encodeURIComponent(carId);
      const res = await fetch(`${restURL}/cars/${escapedCarId}`);
      const oldCar = await res.json();

      await fetch(`${restURL}/cars/${escapedCarId}`, {
        method: 'DELETE',
      });

      return oldCar;
    },
  }
};
