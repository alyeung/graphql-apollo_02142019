export const typeDefs = `

  interface Contact {
    id: Int
    firstName: String
    lastName: String
  }

  type Employee implements Contact {
    id: Int
    firstName: String
    lastName: String
    ssn: String
  }

  type Vendor implements Contact  {
    id: Int
    firstName: String
    lastName: String
    companyName: String
    ein: String    
  }

  union People = Employee | Vendor

  type Query {
    widgets: [Widget]
    widget(widgetId: Int!): Widget
    cars: [Car]
    car(carId: Int!): Car
    authors: [Author]
    books: [Book]
    contacts: [Contact]
    people: [People]
  }

  type Mutation {
    appendWidget(widget: AppendWidget!): Widget
    appendCar(car: AppendCar!): Car
    deleteWidget(widgetId: Int!): Widget
    deleteCar(carId: Int!): Car
  }

  type Author {
    id: Int
    firstName: String
    lastName: String
    phoneNumber: String
    fullName: String
    bookCount: Int
    books: [Book]
  }

  type Book {
    id: Int
    isbn: String
    title: String
    authorId: Int
    author: Author
    category: String
    price: Float
    quantity: Int
  }

  type Widget {
    id: Int
    name: String
    description: String
    quantity: Int
    color: String
    price: Float
  }

  input AppendWidget {
    name: String
    description: String
    quantity: Int
    color: String
    price: Float
  }

  type Car {
    id: Int
    make: String
    model: String
    year: Int
    color: String
    price: Float
  }

  input AppendCar {
    make: String
    model: String
    year: Int
    color: String
    price: Float
  }
`;
